resource "aws_route53_record" "bastion" {
  zone_id = "${var.main_domain_zone_id}"
  name    = "bastion.${var.main_domain}"
  type    = "A"

  alias {
    name                   = "${module.lb.nlb_dns_name}"
    zone_id                = "${module.lb.nlb_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "api-internal" {
  zone_id = "${var.main_domain_zone_id}"
  name    = "api-internal.${var.main_domain}"
  type    = "A"
  ttl     = "1"
  records = ["${module.lb.windows_instance_private_ip}"]
}

resource "aws_route53_record" "win-internal" {
  zone_id = "${var.main_domain_zone_id}"
  name    = "win-internal.${var.main_domain}"
  type    = "A"
  ttl     = "1"
  records = ["${module.lb.windows_instance_private_ip}"]
}

resource "aws_route53_record" "helix-internal" {
  zone_id = "${var.main_domain_zone_id}"
  name    = "helix-internal.${var.main_domain}"
  type    = "A"
  ttl     = "1"
  records = ["${module.lb.helix_instance_private_ip}"]
}

resource "aws_route53_record" "docker-internal" {
  zone_id = "${var.main_domain_zone_id}"
  name    = "docker-internal.${var.main_domain}"
  type    = "A"
  ttl     = "1"
  records = ["${module.lb.docker_instance_private_ip}"]
}
