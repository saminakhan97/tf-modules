module "notify_slack" {
  providers = {
    aws = "aws.global"
  }

  source               = "./../sns_slack_notification"
  sns_topic_name       = "${var.identifier}-slack"
  slack_webhook_url    = "${var.sns_slack_webhook_url}"
  slack_channel        = "${var.sns_slack_channel}"
  slack_username       = "${var.sns_slack_username}"
  lambda_function_name = "${var.identifier}-slack-notify"
}
