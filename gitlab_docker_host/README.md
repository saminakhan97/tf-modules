# How to add a new service

## Roadmap
* Packer with alpine and docker - OK
* nginx - OK
* Install gitlab runner - OK
* Use docker daemon to do remote deploy - OK
* Install vault
* Request certificate - OK
* Create load balancer - OK
* Create route53 records - OK
* Create s3 + cloudfront
* Use gitlab private registry - OK
* Route53 health checks for apps
* Run a script to unregister the Gitlab Runner
* Remove local files, use echo 'x' > file
* Remove key pair and use vault