variable "zone_id" {
  description = "Route53 zone name"
}

variable "main_domain" {
  description = "Main Domain to host the service"
}

variable "identifier" {
  description = "Setup identifier"
}

variable "ssh_public_key" {
  description = "Default ssh public key to connect to the host"
}

variable "subdomains" {
  description = "List of subdomains to request certificates"
  type        = "list"
}

variable "allowed_cidrs" {
  description = "CIDRs allowed to connect via SSH or Docker Daemon"
  type        = "list"

  default = [
    "0.0.0.0/0",
  ]
}

# variable "ami" {
#   description = "Base AMI to use on the host"
# }

variable "aws_region" {
  description = "AWS Region to deploy the resources"
}

# variable "tls_organization" {
#   description = "Organization used on TLS CN"
# }

# variable "tls_country" {
#   description = "Country used on TLS CN"
# }

variable "docker_instance_type" {
  description = "Instance Type to be used on the aws ec2 instance"
}

variable "windows_instance_type" {
  description = "Instance Type to be used on the aws ec2 instance"
}

variable "sns_slack_username" {
  description = "Slack Username to show on the SNS Topic message"
}

variable "sns_slack_webhook_url" {
  description = "Slack WebHook to call for  SNS topic trigger"
}

variable "sns_slack_channel" {
  description = "Slack channel to post the sns topic"
}

variable "user_data" {
  description = "User Data Script to run on first launch"
}

variable "elb_volume_size" {
  default = "8"
}

variable "vpc_id" {}

variable "private_subnet_ids" {
  description = "VPC private subnet ID"
  type        = "list"
}

variable "public_subnet_ids" {
  description = "VPC public subnet ID"
  type        = "list"
}

variable "elb_public_ip_ids" {
  type = "list"
}

variable "vpn_port" {
  default = "1139"
}

variable "acm_certificate_arn" {}

variable "windows_ami" {}

variable "docker_ami" {}

variable "main_domain_zone_id" {}

variable "windows_user_data" {}

variable "vpn_ami" {
  default = "ami-0bb84df8b7f1eea55"
}

variable "vpn_instance_type" {
  default = "t2.micro"
}

variable "vpn_user_data" {}

variable "vpn_volume_size" {
  default = "10"
}

variable "helix_ami" {
  description = "AMI id to deploy"
}

variable "helix_instance_type" {
  default = "t2.medium"
}

variable "helix_user_data" {
  description = "User data script to run after deploy"
}

variable "sql_endpoint" {}
