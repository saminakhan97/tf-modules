output "nlb_dns_name" {
  value = "${module.lb.nlb_dns_name}"
}

output "nlb_zone_id" {
  value = "${module.lb.nlb_zone_id}"
}

output "slack_topic_arn" {
  value = "${module.notify_slack.this_slack_topic_arn}"
}

output "bastion_ssh_port" {
  value = "22"
}

output "this_instance_id" {
  value = "${module.lb.this_instance_id}"
}
