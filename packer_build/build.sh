#!/bin/bash
# ssh_key_generator - designed to work with the Terraform External Data Source provider
#   https://www.terraform.io/docs/providers/external/data_source.html
#  by Irving Popovetsky <irving@popovetsky.com> 
#
#  this script takes the 3 customer_* arguments as JSON formatted stdin
#  produces public_key & private_key (contents) and the private_key_file (path) as JSON formatted stdout
#  DEBUG statements may be safely uncommented as they output to stderr

function error_exit() {
  echo "$1" 1>&2
  exit 1
}

function check_deps() {
  test -f $(which curl) || error_exit "curl command not detected in path, please install it"
  test -f $(which jq) || error_exit "jq command not detected in path, please install it"
  test -f $(which aws) || error_exit "aws command not detected in path, please install it"
}

function parse_input() {
  eval $(jq -r ' . | to_entries[] | "export \(.key | ascii_upcase)=\(.value | @sh)"')
}

function build() {
  script_dir=$(dirname $0)
  export AWS_DEFAULT_REGION=${REGION}
  cd ${PACKER_PATH}
  rm -rf /tmp/existing-${PACKER_FILE} /tmp/ami_id-${PACKER_FILE} /tmp/${PACKER_FILE}.log manifest.json
  echo `aws ec2 describe-images --owners self --filters "Name=name,Values=${AMI_NAME_PREFIX}-${SHA1}"` > /tmp/existing-${PACKER_FILE}
  export EXISTING=$(cat /tmp/existing-${PACKER_FILE})
  if [[ `echo ${EXISTING} | jq -r .Images | jq length` == 1 ]]; then 
    echo $EXISTING | jq -r .Images[0].ImageId > /tmp/ami_id-${PACKER_FILE}
  else
    packer build -var 'aws_region='${REGION} -var='sha1='${SHA1} -var='ami_name_prefix='${AMI_NAME_PREFIX} ${PACKER_FILE} > /tmp/${PACKER_FILE}.log
    cat manifest.json | jq -r '.builds[0].artifact_id' | sed "s#$REGION:##g" > /tmp/ami_id-${PACKER_FILE}
  fi
}

function produce_output() {
  ami_id=$(cat /tmp/ami_id-${PACKER_FILE})
  if [[ -z "${ami_id}" ]]; then 
    
    error_exit "Packer error. Check log at /tmp/${PACKER_FILE}.log"`cat /tmp/${PACKER_FILE}.log`; 
  fi
  jq -n \
    --arg ami_id "$ami_id" \
    '{"ami_id":$ami_id}'
}

# main()
check_deps
parse_input
build
produce_output