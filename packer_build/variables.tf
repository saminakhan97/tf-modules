variable "packer_file" {
  description = "Packer file to build"
}

variable "ami_name_prefix" {
  description = "Prefix used to build the ami"
}

variable "aws_region" {
  description = "AWS Region to publish the packer AMI"
}
