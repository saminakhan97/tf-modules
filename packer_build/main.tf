# data "external" "consul_vault_build" {
#   program = ["bash", "${path.module}/build.sh"]
#   query = {
#     region          = "${var.aws_region}"
#     sha1            = "${sha1(file("${pathexpand(path.root)}/consul-vault/consul-vault.json"))}"
#     packer_file     = "consul-vault.json"
#     ami_name_prefix = "nada-ltd-consul-vault-"
#     aws_region      = "${var.aws_region}"
#     packer_path     = "${pathexpand(path.module)}/consul-vault"
#     packer_var_file = "consul-vault-env.json"
#   }
# }

data "external" "docker_build" {
  program = ["bash", "${path.module}/build.sh"]

  query = {
    region          = "${var.aws_region}"
    sha1            = "${sha1(file("${pathexpand(path.module)}/docker/docker.json"))}"
    packer_file     = "docker.json"
    ami_name_prefix = "${var.ami_name_prefix}"
    aws_region      = "${var.aws_region}"
    packer_path     = "${pathexpand(path.module)}/docker"
  }
}
