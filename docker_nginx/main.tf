data "template_file" "nginx_conf" {
  template = "${file("${path.module}/nginx.conf")}"

  vars = {
    nginx_extra_settings = "${var.nginx_extra_settings}"
    tld                  = "${var.tld}"
    extra_locations      = "${var.extra_locations}"
    main_location_pre    = "${var.main_location_pre}"
    main_location_post   = "${var.main_location_post}"
    read_timeout         = "${var.read_timeout}"
    connect_timeout      = "${var.connect_timeout}"
    proxy_real_cidr      = "${var.proxy_real_cidr}"
  }
}

resource "docker_container" "container" {
  image = "${var.nginx_image}"
  name  = "nginx"

  networks_advanced = "${var.networks}"

  ports = {
    internal = "80"
    external = "80"
  }

  ports = {
    internal = "8080"
    external = "8080"
  }

  upload = {
    content = "${data.template_file.nginx_conf.rendered}"
    file    = "/etc/nginx/nginx.conf"
  }
}
