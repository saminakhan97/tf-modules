worker_processes  1;

events {
    worker_connections  1024;
}
http {
    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }

    resolver_timeout 10s;
    resolver 127.0.0.11 ipv6=off valid=1s;

${nginx_extra_settings}

    server {
        listen 8080 default_server;
        listen [::]:8080 default_server;

        return 301 https://$host$request_uri;
    }

    server {
        listen 80 default_server proxy_protocol;
        listen [::]:80 default_server proxy_protocol;
        server_name ~^(?<service>[a-zA-Z0-9\-]+)\.?(?<domain>.+)$;

        location / {

${main_location_pre}

            if ($http_user_agent ~* "Amazon-Route53-Health-Check-Service" ) {
                access_log off;
            }
            if ($http_user_agent ~* "NewRelicPinger" ) {
                access_log off;
            }
            if ($http_user_agent ~* "Zabbix" ) {
                access_log off;
            }
            proxy_pass http://$service.${tld}:3000;
            proxy_set_header Host $http_host;
            proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_http_version 1.1;
            proxy_read_timeout ${read_timeout};
            proxy_connect_timeout ${connect_timeout};

${main_location_post}

        }

${extra_locations}

        location /socket.io/ {
            proxy_pass http://$service.${tld}:3000;
            proxy_http_version 1.1;
            proxy_set_header Host $host;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
        }

    }
}