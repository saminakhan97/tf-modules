locals {
  main_domain         = "${var.domains[0]}"
  alternative_domains = "${distinct(concat(slice(var.domains, 1, length(var.domains))))}"
}

resource "aws_acm_certificate" "cert" {
  domain_name               = "${local.main_domain}"
  validation_method         = "DNS"
  subject_alternative_names = "${local.alternative_domains}"

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["subject_alternative_names"]
  }

  provisioner "local-exec" {
    command = "sleep 120"
  }
}

# resource "aws_acm_certificate_validation" "cert" {
#   certificate_arn         = "${aws_acm_certificate.cert.arn}"
#   validation_record_fqdns = ["${var.domains}"]
# }

