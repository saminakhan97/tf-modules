variable "domain" {
  description = "Domain for the CN"
}

variable "algorithm" {
  description = "Algorithm to encrypt the certificates"
  default     = "RSA"
}

variable "organization" {
  description = "Organization for the CN"
}

variable "country" {
  description = "Country for the CN"
}

variable "cert_domains" {
  type        = "list"
  description = "Domains (list) for the server certificate request"
}
