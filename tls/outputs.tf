output "ca_cert_pem" {
  value = "${tls_self_signed_cert.ca_crt.cert_pem}"
}

output "ca_key_pem" {
  value = "${tls_private_key.ca_key.private_key_pem}"
}

output "server_key_pem" {
  value = "${tls_private_key.server_key.private_key_pem}"
}

output "server_cert_pem" {
  value = "${tls_locally_signed_cert.server_crt.cert_pem}"
}

output "client_cert_pem" {
  value = "${tls_locally_signed_cert.client_crt.cert_pem}"
}

output "client_key_pem" {
  value = "${tls_private_key.client_key.private_key_pem}"
}
