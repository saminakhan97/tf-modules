resource "tls_private_key" "ca_key" {
  algorithm = "${var.algorithm}"
}

resource "tls_self_signed_cert" "ca_crt" {
  key_algorithm     = "${tls_private_key.ca_key.algorithm}"
  private_key_pem   = "${tls_private_key.ca_key.private_key_pem}"
  is_ca_certificate = true

  subject {
    common_name  = "*.${var.domain}"
    organization = "${var.organization}"
    country      = "${var.country}"
  }

  validity_period_hours = 43800
  is_ca_certificate     = true

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
    "cert_signing",
  ]
}

resource "tls_private_key" "server_key" {
  algorithm = "${var.algorithm}"
}

resource "tls_cert_request" "server_csr" {
  key_algorithm   = "${tls_private_key.server_key.algorithm}"
  private_key_pem = "${tls_private_key.server_key.private_key_pem}"

  subject {
    common_name  = "*.${var.domain}"
    organization = "${var.organization}"
    country      = "${var.country}"
  }

  ip_addresses = ["0.0.0.0", "127.0.0.1"]

  dns_names = "${var.cert_domains}"
}

resource "tls_locally_signed_cert" "server_crt" {
  cert_request_pem   = "${tls_cert_request.server_csr.cert_request_pem}"
  ca_key_algorithm   = "${var.algorithm}"
  ca_private_key_pem = "${tls_private_key.ca_key.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.ca_crt.cert_pem}"

  validity_period_hours = 43800

  allowed_uses = [
    "server_auth",
  ]
}

resource "tls_private_key" "client_key" {
  algorithm = "${var.algorithm}"
}

resource "tls_cert_request" "client_csr" {
  key_algorithm   = "${tls_private_key.client_key.algorithm}"
  private_key_pem = "${tls_private_key.client_key.private_key_pem}"

  subject {
    common_name = "client"
  }
}

resource "tls_locally_signed_cert" "client_crt" {
  cert_request_pem   = "${tls_cert_request.client_csr.cert_request_pem}"
  ca_key_algorithm   = "${var.algorithm}"
  ca_private_key_pem = "${tls_private_key.ca_key.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.ca_crt.cert_pem}"

  validity_period_hours = 43800

  allowed_uses = [
    "client_auth",
  ]
}
