variable "port" {}

variable "identifier" {}

variable "nlb_arn" {}

variable "vpc_id" {}

variable "alb_dns_name" {}
