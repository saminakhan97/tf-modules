output "arn" {
  value = "${data.aws_kms_key.root.arn}"
}

output "id" {
  value = "${data.aws_kms_key.root.id}"
}
