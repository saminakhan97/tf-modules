resource "aws_security_group" "sg" {
  name        = "${var.identifier}-rds-sg"
  description = "Managed by Terraform"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["${var.allowed_cidrs}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-rds"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

data "aws_s3_bucket" "bucket" {
  bucket = "${var.backup_bucket}"
}

resource "random_id" "password" {
  keepers {
    timestamp = "${var.username}"
  }

  byte_length = 8
}

module "db" {
  source                          = "git::https://github.com/gp-technical/terraform-aws-rds?ref=v1.28.2"
  identifier                      = "${var.identifier}"
  engine                          = "sqlserver-web"
  engine_version                  = "${var.engine_version}"
  instance_class                  = "${var.instance_type}"
  allocated_storage               = "${var.volume_size}"
  performance_insights_enabled    = true
  name                            = ""
  username                        = "${var.username}"
  password                        = "${random_id.password.dec}"
  port                            = "1433"
  vpc_security_group_ids          = ["${aws_security_group.sg.id}"]
  maintenance_window              = "${var.maintenance_window}"
  backup_window                   = "03:00-06:00"
  monitoring_interval             = "30"
  monitoring_role_name            = "${var.identifier}-monitoring"
  create_monitoring_role          = true
  publicly_accessible             = false
  backup_retention_period         = 7
  skip_final_snapshot             = false
  subnet_ids                      = "${var.subnet_ids}"
  final_snapshot_identifier       = "final-${var.identifier}"
  create_db_parameter_group       = false
  license_model                   = "license-included"
  deletion_protection             = "${var.deletion_protection}"
  major_engine_version            = "${var.major_engine_version}"
  kms_key_id                      = "${var.kms_key_id}"
  storage_encrypted               = true
  enabled_cloudwatch_logs_exports = "${var.enabled_cloudwatch_logs_exports}"

  options = [
    {
      option_name = "SQLSERVER_BACKUP_RESTORE"

      option_settings = [
        {
          name  = "IAM_ROLE_ARN"
          value = "arn:aws:iam::${var.account_id}:role/${var.identifier}-rds-role"
        },
      ]
    },
  ]

  tags = {
    Creator     = "marcelomarra"
    Environment = "${var.identifier}"
    Owner       = "DevSquad"
    Terraform   = "true"
  }
}
