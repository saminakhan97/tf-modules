data "aws_iam_policy_document" "rds" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["rds.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "bucket" {
  statement {
    actions = ["s3:ListBucket",
      "s3:GetBucketLocation",
    ]

    resources = ["arn:aws:s3:::${data.aws_s3_bucket.bucket.id}"]
  }
}

data "aws_iam_policy_document" "objects" {
  statement {
    actions = ["s3:GetObjectMetaData",
      "s3:GetObject",
      "s3:PutObject",
      "s3:ListMultipartUploadParts",
      "s3:AbortMultipartUpload",
    ]

    resources = ["arn:aws:s3:::${data.aws_s3_bucket.bucket.id}/*"]
  }
}

resource "aws_iam_role" "rds" {
  name = "${var.identifier}-rds-role"

  assume_role_policy = "${data.aws_iam_policy_document.rds.json}"
}

resource "aws_iam_policy" "bucket" {
  name = "${var.identifier}-bucket"

  policy = "${data.aws_iam_policy_document.bucket.json}"
}

resource "aws_iam_policy" "objects" {
  name = "${var.identifier}-objects"

  policy = "${data.aws_iam_policy_document.objects.json}"
}

resource "aws_iam_role_policy_attachment" "bucket" {
  role       = "${aws_iam_role.rds.name}"
  policy_arn = "${aws_iam_policy.bucket.arn}"
}

resource "aws_iam_role_policy_attachment" "objects" {
  role       = "${aws_iam_role.rds.name}"
  policy_arn = "${aws_iam_policy.objects.arn}"
}
