output "client_configuration_file" {
  value = "${var.vpn_client_name}.ovpn"
}

output "closing_message" {
  value = "Your VPN is ready! Check out client configuration file to configure your client! Have fun!'"
}
