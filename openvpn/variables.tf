variable "ssh_remote_user" {
  default = "docker"
}

variable "vpn_data" {}

variable "vpn_port" {
  default = 1194
}

variable "vpn_client_name" {
  default = "devsquad@gp.aws.amazon.com"
}

variable "identifier" {}
