output "bucket_arn" {
  value = "${aws_s3_bucket.bucket.arn}"
}

output "bucket_id" {
  value = "${aws_s3_bucket.bucket.id}"
}

output "lock_arn" {
  value = "${aws_dynamodb_table.lock.arn}"
}

output "lock_id" {
  value = "${aws_dynamodb_table.lock.id}"
}

output "lock_stram_arn" {
  value = "${aws_dynamodb_table.lock.stream_arn}"
}
