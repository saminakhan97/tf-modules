resource "aws_s3_bucket" "bucket" {
  bucket = "gp-tf-backend-${var.identifier}"
  acl    = "private"

  tags = {
    Creator     = "marcelomarra"
    Environment = "${var.identifier}"
    Owner       = "DevSquad"
    Terraform   = "true"
  }

  lifecycle {
    prevent_destroy = true
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${var.kms_arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "aws_dynamodb_table" "lock" {
  name         = "gp-tf-backend-${var.identifier}-lock"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute = {
    name = "LockID"
    type = "S"
  }

  tags = {
    Creator     = "marcelomarra"
    Environment = "${var.identifier}"
    Owner       = "DevSquad"
    Terraform   = "true"
  }
}
