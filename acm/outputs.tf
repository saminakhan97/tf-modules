output "main_domain" {
  value = "${var.domains[0]}"
}

output "aws_acm_certificate_arn" {
  value = "${aws_acm_certificate.cert.arn}"
}
