resource "docker_image" "image" {
  name = "${var.image}"
}

resource "docker_container" "container" {
  image = "${docker_image.image.latest}"
  name  = "${local.fqdn}"

  networks_advanced = {
    name = "${var.main_domain}"
  }

  env = "${var.env_list}"
}
