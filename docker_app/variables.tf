variable "main_domain" {
  description = "Top level domain of the app"
}

variable "elb_dns" {
  description = "Elastic Load Balancer DNS Name"
}

variable "elb_zone_id" {
  description = "Elastic Load Balancer Zone id"
}

variable "image" {
  description = "Docker image name for the app"
}

variable "env_list" {
  description = "Env var list to run the app"
  type        = "list"
}

variable "name" {
  description = "App name"
}

variable "alarm_sns_topic" {}
