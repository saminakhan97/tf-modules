locals {
  fqdn = "${var.name}.${var.main_domain}"
}

module "app" {
  source          = "./../app"
  elb_dns         = "${var.elb_dns}"
  elb_zone_id     = "${var.elb_zone_id}"
  name            = "${var.name}"
  main_domain     = "${var.main_domain}"
  alarm_sns_topic = "${var.alarm_sns_topic}"
}
