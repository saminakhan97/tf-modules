data "aws_s3_bucket" "bucket" {
  bucket = "${var.vault_backend_s3_bucket}"
}

data "template_file" "vault_env_template" {
  template = "$${bucket}"

  vars {
    bucket = "${data.aws_s3_bucket.bucket.id}"
  }
}

resource "docker_image" "image" {
  name = "vault"
}

resource "docker_container" "container" {
  image = "${docker_image.image.latest}"
  name  = "vault"

  networks_advanced = {
    name = "vault"
  }

  networks_advanced = {
    name = "runner"
  }

  ports = {
    internal = "8200"
    external = "8200"
  }

  ports = {
    internal = "8201"
    external = "8201"
  }

  command = ["vault", "server", "-config", "/vault/config/local.json"]

  capabilities {
    add = ["IPC_LOCK"]
  }

  privileged = true

  env = [
    "AWS_S3_BUCKET=${data.template_file.vault_env_template.rendered}",
    "AWS_ACCESS_KEY_ID=${var.aws_access_key_id}",
    "AWS_SECRET_ACCESS_KEY=${var.aws_secret_access_key}",
    "AWS_DEFAULT_REGION=${var.aws_region}",
    "VAULT_SKIP_VERIFY=1",
    "VAULT_ADDR=http://127.0.0.1:8200",
  ]

  upload = {
    content = "${file("${path.module}/vault.config.hcl")}"
    file    = "/vault/config/local.json"
  }
}
