variable "main_domain" {}

variable "name" {}

variable "proxy_destination" {}

variable "port" {}

variable "host" {}

variable "extra_locations" {
  default = ""
}

variable "main_location_pre" {
  default = ""
}

variable "main_location_post" {
  default = ""
}

variable "nginx_image" {}

variable "read_timeout" {
  default = "15s"
}

variable "connect_timeout" {
  default = "5s"
}
