data "template_file" "conf" {
  template = "${file("${path.module}/nginx.conf")}"

  vars {
    proxy_destination  = "${var.proxy_destination}"
    port               = "${var.port}"
    host               = "${var.host}"
    extra_locations    = "${var.extra_locations}"
    main_location_pre  = "${var.main_location_pre}"
    main_location_post = "${var.main_location_post}"
    read_timeout       = "${var.read_timeout}"
    connect_timeout    = "${var.connect_timeout}"
  }
}

output "name" {
  value = "${data.template_file.conf.rendered}"
}

resource "docker_container" "container" {
  image = "${var.nginx_image}"
  name  = "${var.name}"

  ports = {
    internal = "${var.port}"
  }

  networks_advanced = {
    name = "${var.main_domain}"
  }

  upload = {
    content = "${data.template_file.conf.rendered}"
    file    = "/etc/nginx/nginx.conf"
  }
}
