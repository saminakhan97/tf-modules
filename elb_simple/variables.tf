variable "identifier" {
  description = "A identifier for AWS Resources"
}

variable "ami" {
  description = "AMI id to deploy"
}

variable "windows_ami" {
  description = "AMI id to deploy"
}

variable "acm_certificate_arn" {
  description = "ACM Certificate ARN for HTTPS"
}

variable "ssh_public_key" {
  description = "SSH Key to log in on the instance"
}

variable "elb_security_group_ids" {
  description = "Additional security groups (ids) for the elb"
  type        = "list"
  default     = []
}

variable "instance_security_group_names" {
  description = "Additional security groups (names) for the instance"
  type        = "list"
  default     = []
}

variable "user_data" {
  description = "User data script to run after deploy"
}

variable "windows_user_data" {
  description = "User data script to run after deploy"
}

variable "additional_elb_listeners" {
  type        = "list"
  description = "Additional listener configs for the elb"
  default     = []
}

variable "docker_instance_type" {
  default = "t2.micro"
}

variable "windows_instance_type" {
  default = "t2.micro"
}

variable "volume_size" {
  default = "8"
}

variable "vpc_id" {
  description = "VPC ID"
}

variable "private_subnet_ids" {
  description = "VPC private subnet ID"
  type        = "list"
}

variable "public_subnet_ids" {
  description = "VPC public subnet ID"
  type        = "list"
}

variable "elb_public_ip_ids" {
  type = "list"
}

variable "zone_id" {}

variable "main_domain" {}

variable "vpn_ami" {}

variable "vpn_instance_type" {}

variable "vpn_user_data" {}

variable "vpn_volume_size" {}

variable "helix_ami" {
  description = "AMI id to deploy"
}

variable "helix_instance_type" {
  default = "t2.medium"
}

variable "helix_user_data" {
  description = "User data script to run after deploy"
}

variable "sql_endpoint" {}
