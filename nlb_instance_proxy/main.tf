resource "aws_lb_target_group" "group" {
  name        = "${var.identifier}-nlb-proxy"
  port        = "${var.port}"
  protocol    = "TCP"
  vpc_id      = "${var.vpc_id}"
  target_type = "instance"

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = "${var.nlb_arn}"
  port              = "${var.port}"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.group.arn}"
  }
}

resource "aws_lb_target_group_attachment" "attachment" {
  target_group_arn = "${aws_lb_target_group.group.arn}"
  target_id        = "${var.instance_id}"
  port             = "${var.port}"
}
