resource "aws_instance" "instance" {
  ami                         = "${var.ami}"
  instance_type               = "${var.instance_type}"
  subnet_id                   = "${var.subnet_id}"
  user_data                   = "${var.user_data}"
  associate_public_ip_address = "${var.associate_public_ip_address}"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${var.vpc_security_group_ids}"]
  monitoring                  = true
  ebs_optimized               = "${var.ebs_optimized}"
  iam_instance_profile        = "${var.iam_instance_profile}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-${var.instance_name}"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }

  volume_tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}-${var.instance_name}"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }

  root_block_device {
    volume_size = "${var.volume_size}"
    volume_type = "gp2"
  }
}

# resource "aws_ebs_volume" "volume" {
#   availability_zone = "${var.availability_zone}"
#   size              = "${var.volume_size}"
#   type              = "standard"
#   encrypted         = true
#   kms_key_id        = "${var.kms_key_id}"


#   tags = {
#     Application     = "${var.identifier}"
#     Confidentiality = "StrictlyConfidential"
#     Creator         = "marcelomarra"
#     Environment     = "${var.identifier}"
#     Name            = "${var.identifier}-${var.instance_name}"
#     Owner           = "DevSquad"
#     Product         = "${var.identifier}"
#   }
# }


# resource "aws_volume_attachment" "attachment" {
#   device_name = "${var.device_name}"
#   volume_id   = "${aws_ebs_volume.volume.id}"
#   instance_id = "${aws_instance.instance.id}"
# }

