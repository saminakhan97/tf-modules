variable "instance_name" {}

variable "identifier" {}

variable "user_data" {}
variable "subnet_id" {}

variable "instance_type" {}
variable "ami" {}

variable "vpc_security_group_ids" {
  type = list(string)
}

variable "key_name" {}

variable "kms_key_id" {}

variable "availability_zone" {}

variable "associate_public_ip_address" {
  default = "false"
}

variable "volume_size" {}

variable "device_name" {
  default = "/dev/sda1"
}

variable "ebs_optimized" {
  default = false
}

variable "iam_instance_profile" {
  default = ""
}
