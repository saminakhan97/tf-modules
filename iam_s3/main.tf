data "aws_s3_bucket" "bkp_bucket" {
  bucket = "${var.bucket_name}"
}

data "aws_iam_policy_document" "bkp_bucket" {
  statement {
    actions = ["s3:ListBucket",
      "s3:GetBucketLocation",
    ]

    resources = ["arn:aws:s3:::${data.aws_s3_bucket.bkp_bucket.id}"]
  }
}

data "aws_iam_policy_document" "bkp_objects" {
  statement {
    actions = ["s3:GetObjectMetaData",
      "s3:GetObject",
      "s3:PutObject",
      "s3:ListMultipartUploadParts",
      "s3:AbortMultipartUpload",
    ]

    resources = ["arn:aws:s3:::${data.aws_s3_bucket.bkp_bucket.id}/*"]
  }
}

resource "aws_iam_role" "role" {
  name = "s3-${var.bucket_name}-role"
}

resource "aws_iam_policy" "bucket" {
  name = "s3-${var.bucket_name}-bucket-policy"

  policy = "${data.aws_iam_policy_document.bkp_bucket.json}"
}

resource "aws_iam_policy" "objects" {
  name = "s3-${var.bucket_name}-objects-policy"

  policy = "${data.aws_iam_policy_document.bkp_objects.json}"
}

resource "aws_iam_role_policy_attachment" "bkp_bucket" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "${aws_iam_policy.bucket.arn}"
}

resource "aws_iam_role_policy_attachment" "bkp_objects" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "${aws_iam_policy.objects.arn}"
}
