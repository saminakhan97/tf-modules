variable "main_domain" {
  description = "Top level domain of the app"
}

variable "elb_dns" {
  description = "Elastic Load Balancer DNS Name"
}

variable "elb_zone_id" {
  description = "Elastic Load Balancer Zone id"
}

variable "name" {
  description = "App name"
}

variable "health_check_path" {
  default = "/"
}

variable "alarm_sns_topic_arn" {}

variable "identifier" {}

variable "main_domain_zone_id" {}
