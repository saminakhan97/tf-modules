resource "aws_route53_record" "alb" {
  zone_id = "${var.main_domain_zone_id}"
  name    = "${local.fqdn}"
  type    = "A"

  alias {
    name                   = "${var.elb_dns}"
    zone_id                = "${var.elb_zone_id}"
    evaluate_target_health = false
  }
}
