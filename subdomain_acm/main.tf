locals {
  domain_validation_options = "${flatten(aws_acm_certificate.cert.*.domain_validation_options)}"
}

resource "aws_acm_certificate" "cert" {
  domain_name       = "${var.main_domain}"
  validation_method = "DNS"

  subject_alternative_names = ["${
    formatlist("%s.%s",
      var.subdomains,
      var.main_domain
    )
  }"]
}

resource "aws_route53_record" "cert_validation" {
  count   = "${(length(var.subdomains) + 1)}"
  zone_id = "${var.zone_id}"
  ttl     = 1
  name    = "${lookup(local.domain_validation_options[count.index], "resource_record_name")}"
  type    = "${lookup(local.domain_validation_options[count.index], "resource_record_type")}"
  records = ["${lookup(local.domain_validation_options[count.index], "resource_record_value")}"]
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = "${aws_acm_certificate.cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert_validation.*.fqdn}"]
}
