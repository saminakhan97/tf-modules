resource "aws_lb_target_group" "group" {
  name        = "${var.identifier}-nlb-proxy"
  port        = "${var.port}"
  protocol    = "TCP"
  vpc_id      = "${var.vpc_id}"
  target_type = "ip"

  stickiness {
    type    = "lb_cookie"
    enabled = false
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = "${var.nlb_arn}"
  port              = "${var.port}"
  protocol          = "TLS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${var.acm_certificate_arn}"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.group.arn}"
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "gp-${var.identifier}-nlb-proxy"
  acl    = "private"

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}

module "alb_static_ip_lambda" {
  source       = "git::https://github.com/pbar1/terraform-aws-lb-linker.git?ref=v1.0.0"
  alb_dns_name = "${var.alb_dns_name}"
  name         = "${var.identifier}-nlb-proxy"
  nlb_tg_arn   = "${aws_lb_target_group.group.arn}"
  s3_bucket    = "${aws_s3_bucket.bucket.id}"
  alb_listener = "${var.port}"

  tags = {
    Application     = "${var.identifier}"
    Confidentiality = "StrictlyConfidential"
    Creator         = "marcelomarra"
    Environment     = "${var.identifier}"
    Name            = "${var.identifier}"
    Owner           = "DevSquad"
    Product         = "${var.identifier}"
  }
}
